import curses


class Console:
    def __init__(self):
        # todo test run config console emulation = false
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.nonl()
        curses.start_color()
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
        self.stdscr.keypad(True)

    def clear(self):
        self.stdscr.clear()

    def new_line(self):
        self.stdscr.addstr('\n')

    # todo only 18 lines
    def write_line(self, text, effect=0):
        self.write(text, effect)
        self.new_line()

    def write(self, text, effect=0):
        self.stdscr.addstr(str(text), effect)

    def get_key(self):
        return self.stdscr.getch()

    def reset(self):
        self.stdscr.keypad(0)
        curses.echo()
        curses.nocbreak()
        curses.endwin()
