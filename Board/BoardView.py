import curses

from Board.BoardModel import BoardModel
from Console import Console


class BoardView:
    def __init__(self, console: Console):
        self.console = console

    def show(self, board: BoardModel):
        for column in reversed(range(BoardModel.height)):
            self.console.write('┃')
            for row in range(BoardModel.width):
                if 1 == board.grid[row][column]:
                    self.console.write('●', curses.color_pair(1))
                elif -1 == board.grid[row][column]:
                    self.console.write('●', curses.color_pair(2))
                else:
                    self.console.write(' ')
                self.console.write('┃')
            self.console.new_line()
        self.console.new_line()
