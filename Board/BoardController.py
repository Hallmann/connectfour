from Board.BoardView import BoardView
from Console import Console

from Board.BoardModel import BoardModel


class BoardController:
    def __init__(self, console: Console):
        self.board = BoardModel()
        self.view = BoardView(console)

    def show(self):
        self.view.show(self.board)

    def play(self, selected_column: int, player: int):
        self.board = self.board.insert_piece(selected_column, player)

    def check_victory(self, player):
        return self.board.check_victory(player)
