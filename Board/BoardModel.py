from copy import copy


class BoardModel:
    width = 7
    height = 6

    def __init__(self):
        self.grid = [[0 for _ in range(BoardModel.height)] for _ in range(BoardModel.width)]

    def insert_piece(self, column, player):
        for row in range(BoardModel.height):
            if not self.grid[column][row]:
                c = copy(self)
                c.grid[column][row] = player
                return c
        raise AttributeError

    def check_victory(self, player):
        for row in range(BoardModel.height):
            pass
