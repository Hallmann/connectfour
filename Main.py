import curses

from Console import Console

from Board.BoardController import BoardController

if __name__ == '__main__':
    console = Console()
    try:
        controller = BoardController(console)
        debug = ''
        player = 1
        selected_col = 0

        while True:
            console.clear()
            console.write_line(' ' * (1 + (selected_col * 2)) + '▼', curses.A_BLINK)
            controller.show()
            console.write_line(debug)
            key_code = console.get_key()
            if key_code == curses.KEY_LEFT:
                selected_col = max(0, selected_col - 1)
            elif key_code == curses.KEY_RIGHT:
                selected_col = min(6, selected_col + 1)
            elif key_code in (ord('\r'), 13):  # enter
                try:
                    controller.play(selected_col, player)
                    player *= -1
                except:
                    pass
    finally:
        console.reset()
